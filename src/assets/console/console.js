/**
 * Stringify.
 * Inspect native browser objects and functions.
 */
let stringify = (function () {

  let sortCi = function (a, b) {
    return a.toLowerCase() < b.toLowerCase() ? -1 : 1;
  };

  let htmlEntities = function (str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
  };

  /**
   * Recursively stringify an object. Keeps track of which objects it has
   * visited to avoid hitting circular references, and a buffer for indentation.
   * Goes 2 levels deep.
   */
  return function stringify(o, visited = [], buffer = '') {
    let i, vi, type = '', parts = [], circular = false;

    // Get out fast with primitives that don't like toString
    if (o === null) {
      return 'null';
    }
    if (typeof o === 'undefined') {
      return 'undefined';
    }

    // Determine the type
    try {
      type = ({}).toString.call(o);
    } catch (e) { // only happens when typeof is protected (...randomly)
      type = '[object Object]';
    }

    // Handle the primitive types
    if (type === '[object Number]') {
      return '' + o;
    }
    if (type === '[object Boolean]') {
      return o ? 'true' : 'false';
    }
    if (type === '[object Function]') {
      return o.toString().split('\n  ').join('\n' + buffer);
    }
    if (type === '[object String]') {
      return '"' + htmlEntities(o.replace(/"/g, '\\"')) + '"';
    }

    // Check for circular references
    for (vi = 0; vi < visited.length; vi++) {
      if (o === visited[vi]) {
        // Notify the user that a circular object was found and, if available,
        // show the object's outerHTML (for body and elements)
        return '[circular ' + type.slice(1) +
          ('outerHTML' in o ? ' :\n' + htmlEntities(o.outerHTML).split('\n').join('\n' + buffer) : '')
      }
    }

    // Remember that we visited this object
    visited.push(o);

    // Stringify each member of the array
    if (type === '[object Array]') {
      for (i = 0; i < o.length; i++) {
        parts.push(stringify(o[i], visited));
      }
      return '[' + parts.join(', ') + ']';
    }

    // Fake array – very tricksy, get out quickly
    if (type.match(/Array/)) {
      return type;
    }

    let typeStr = type + ' ',
      newBuffer = buffer + '  ';

    // Dive down if we're less than 2 levels deep
    if (buffer.length / 2 < 2) {

      let names = [];
      // Some objects don't like 'in', so just skip them
      try {
        for (i in o) {
          names.push(i);
        }
      } catch (e) {
      }

      names.sort(sortCi);
      for (i = 0; i < names.length; i++) {
        try {
          parts.push(newBuffer + names[i] + ': ' + stringify(o[names[i]], visited, newBuffer));
        } catch (e) {
        }
      }

    }

    // If nothing was gathered, return empty object
    if (!parts.length) return typeStr + '{ ... }';

    // Return the indented object with new lines
    return typeStr + '{\n' + parts.join(',\n') + '\n' + buffer + '}';
  };
}());


const cssCode = `
body #console,
body.console-minimized #console {
    display: block;
}

body.console-minimized #console {
    min-height: 30px;
    cursor: pointer
}

body.console-minimized #console #console-output {
    height: 0;
    overflow: hidden;
    display: none;
}

body.console-minimized #console #console-input,
body.console-minimized #console .consoleActions {
    display: none
}


#console {
    position: fixed;
    bottom: 0;
    left: 0;
    background: #1a1d21;
    width: 100%;
    color: #cfd0d1;
    border-top: 1px solid #2c2f34;
    margin: 0;
    padding: 0;
    font-size: 11px;
    overflow: auto;
    display: none;
    font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen,Ubuntu,Cantarell,"Fira Sans","Droid Sans","Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
}

#console .header {
    border-bottom: 1px solid #2c2f34;
    position: sticky;
    top: 0;
    left: 0;
    height: 30px;
    padding-left: 10px;
    background: #1f2227;
    display: flex;
    align-items: center
}

#console .header svg.logo {
    stroke: #4b4d51;
    margin-right: 5px
}

#console .header .consoleActions {
    position: absolute;
    right: 0;
    top: 0;
    display: flex
}

#console .header .consoleActions a {
    display: flex;
    height: 30px;
    align-items: center;
    justify-content: center;
    color: #cfd0d1;
    margin-right: 10px;
    outline: none;
    text-decoration: none;
}
#console ul {
    max-height: 100px;
    overflow: auto;
}
#console li {
    border-bottom: 1px solid #2c2f34;
    padding: 7px 12px;
    font-family: "SF Mono","Monaco","Andale Mono","Lucida Console","Bitstream Vera Sans Mono","Courier New",Courier,monospace;
    white-space: pre
}

#console li.error {
    background: rgba(191,91,116,.2);
    border-left: 3px solid #bf5b74;
    color: #fbfbfb
}

#console li.error .gotoLine {
    border-radius: 3px;
    border: 1px solid hsla(0,0%,98%,.2);
    color: #fbfbfb;
    padding: 1px 4px;
    font-size: 10px
}

#console li.error .gotoLine:hover {
    background: #bf5b74;
    border: 1px solid #bf5b74
}

#console li.warn {
    background: rgba(243,202,99,.2);
    border-left: 3px solid #f3ca63
}

#console li.info {
    background: rgba(136,210,252,.2);
    border-left: 3px solid #88d2fc
}

#console li.system {
    color: #2e71ff
}

#console-summary {
    justify-content: center;
    align-items: center;
    color: #4b4d51
}

#console-summary,
#console-summary span {
    margin-left: 10px;
    display: flex
}

#console-summary span svg {
    height: 12px;
    stroke: #4b4d51;
    margin-right: 3px
}

#console-summary span.log.active {
    color: #cfd0d1
}

#console-summary span.log.active svg {
    stroke: #cfd0d1
}

#console-summary span.warn.active {
    color: #f3ca63
}

#console-summary span.warn.active svg {
    stroke: #f3ca63
}

#console-summary span.info.active {
    color: #88d2fc
}

#console-summary span.info.active svg {
    stroke: #88d2fc
}

#console-summary span.error.active {
    color: #bf5b74
}

#console-summary span.error.active svg {
    stroke: #bf5b74
}

#console-input {
    position: sticky;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 34px;
    outline: none;
    box-sizing: border-box;
    vertical-align: middle;
    appearance: none;
    border: none;
    box-shadow: 0 -1px 0 #2c2f34,0 1px 0 #2c2f34;
    padding: 7px 12px;
    margin: 0;
    font-family: "SF Mono","Monaco","Andale Mono","Lucida Console","Bitstream Vera Sans Mono","Courier New",Courier,monospace;
    white-space: pre;
    color: #cfd0d1;
    border-radius: 0;
    position: sticky;
    bottom: 0;
    left: 0;
    font-size: 11px;
    background: #141619
}

`;

const customConsole = (w) => {
  const pushToConsole = (payload, type) => {
    w.postMessage({
      console: {
        payload: stringify(payload),
        type: type
      }
    }, "*")
  }

  w.onerror = (message, url, line, column) => {
    // the line needs to correspond with the editor panel
    // unfortunately this number needs to be altered every time this view is changed
    line = line - 70
    if (line < 0) {
      pushToConsole(message, "error")
    } else {
      pushToConsole(`[${line}:${column}] ${message}`, "error")
    }
  }

  let console = (function (systemConsole) {
    return {
      log: function () {
        let args = Array.from(arguments);
        pushToConsole(args, "log");
        systemConsole.log.apply(this, args);
      },
      info: function () {
        let args = Array.from(arguments);
        pushToConsole(args, "info");
        systemConsole.info.apply(this, args);
      },
      warn: function () {
        let args = Array.from(arguments);
        pushToConsole(args, "warn");
        systemConsole.warn.apply(this, args);
      },
      error: function () {
        let args = Array.from(arguments);
        pushToConsole(args, "error");
        systemConsole.error.apply(this, args);
      },
      system: function (arg) {
        pushToConsole(arg, "system");
      },
      clear: function () {
        systemConsole.clear.apply(this, {});
      },
      time: function () {
        let args = Array.from(arguments);
        systemConsole.time.apply(this, args);
      },
      assert: function (assertion, label) {
        if (!assertion) {
          pushToConsole(label, "log");
        }

        let args = Array.from(arguments);
        systemConsole.assert.apply(this, args);
      }
    }
  }(window.console))

  window.console = {...window.console, ...console};
}

class DevlabConsole {
  constructor() {
    window.addEventListener("message", (e) => {
        if (e.origin) {
          if (e.data.console) {
            let t = e.data.console.type;
            let n = (() => {
              if ("system" === t) {
                return `☁️ ${e.data.console.payload}`;
              }
              if ("error" === t) {
                return this.addLineLink(e.data.console.payload);
              } else {
                return this.format(e.data.console.payload);
              }
            })();
            this.pushOutput(n, t);
          }
        }
      }
    );
    this.commandsHistory = [];
    this.historyLocation = this.commandsHistory.length;
    this.element = {
      resultsPanel: document.querySelector("body"),
      console: null,
      summary: null,
      output: null,
      header: null,
      clear: null,
      input: null
    };
    this.loadCSS();
    this.createLayout();
  }

  loadCSS() {
    const style = document.createElement('style');
    style.textContent = cssCode;

    document.head.appendChild(style);
  }

  createLayout() {
    this.element.console = document.createElement("div");
    this.element.console.id = "console";
    this.isVisible() || this.element.resultsPanel.classList.add("console-minimized");
    this.element.output = document.createElement("ul");
    this.element.output.id = "console-output";
    this.element.input = document.createElement("input");
    this.element.input.id = "console-input";
    this.element.input.type = "text";
    this.element.input.placeholder = ">_";
    this.element.input.addEventListener("keyup", e => {
        if (13 === e.keyCode) {
          e.stopPropagation();
          e.preventDefault();
          let t = e.target.value;
          window.postMessage({
            console: {
              payload: t
            }
          }, window.origin);
          this.addHistory(t);
          e.target.value = "";
        }
        38 === e.keyCode && this.previousHistory();
        40 === e.keyCode && this.nextHistory();
      }
    );
    this.element.header = document.createElement("section");
    this.element.header.classList.add("header");
    this.element.header.innerHTML = '\n      <svg class="logo" height="16" viewBox="0 0 24 24"><g stroke-linecap="round" stroke-width="1.8" fill="none" stroke-linejoin="round"><polyline points="4,17 10,11 4,5"></polyline><line x1="12" x2="20" y1="19" y2="19"></line></g></svg>\n      <h3>Console (beta)</h3>\n      <div class="consoleActions">\n        <a id="console-clear" href="#">Clear console</a>\n        <a id="console-minimize" href="#">Minimize</a>\n      </div>\n    ';
    this.element.header.addEventListener("click", () => {
        if (this.element.resultsPanel.classList.contains("console-minimized")) {
          this.show();
          this.element.input.focus();
        }
      }
    );
    this.element.summary = document.createElement("div");
    this.element.summary.id = "console-summary";
    this.element.resultsPanel.append(this.element.console);
    this.element.console.append(this.element.header);
    this.element.header.append(this.element.summary);
    this.element.console.append(this.element.output);
    this.element.console.append(this.element.input);
    this.element.clear = this.element.console.querySelector("#console-clear");
    this.element.clear.addEventListener("click", this.clear.bind(this));
    this.element.minimize = this.element.console.querySelector("#console-minimize");
    this.element.minimize.addEventListener("click", this.minimize.bind(this));
    this.pushOutput("Welcome to DEVLAB Console.", "system");
  }

  updateInput() {
    if (this.commandsHistory[this.historyLocation]) {
      this.element.input.value = this.commandsHistory[this.historyLocation];
    }
  }

  previousHistory() {
    if (this.historyLocation >= 0) {
      this.historyLocation = this.historyLocation - 1;
      this.updateInput()
    }
  }

  nextHistory() {
    if (this.historyLocation < this.commandsHistory.length) {
      this.historyLocation = this.historyLocation + 1;
      this.updateInput();
    }
  }

  addHistory(e) {
    this.commandsHistory.push(e)
  }

  minimize(e) {
    e.preventDefault();
    e.stopPropagation();
    this.hide();
  }

  clear(e) {
    if (e) {
      e.preventDefault();
      e.stopPropagation();
      this.element.output.innerHTML = "";
      this.updateSummary();
    }
  }

  isVisible() {
    return localStorage.getItem("console-visible");
  }

  hide() {
    localStorage.removeItem("console-visible");
    this.element.resultsPanel.classList.add("console-minimized");
  }

  show() {
    localStorage.setItem("console-visible", !0);
    this.element.resultsPanel.classList.remove("console-minimized");
  }

  addLineLink(e) {
    return e = e.replace(/\[(.*)\]/gi, "<a class='gotoLine' href='#$1'>$1</a>");
  }

  pushOutput(e, t) {
    let n = document.createElement("li");
    n.textContent = e;
    n.classList.add(t);
    this.element.output.append(n);
    this.updateSummary();
    this.element.console.scrollTo(0, this.element.console.scrollHeight);
    this.linkErrorLine(n);
  }

  linkErrorLine(e) {
    let t = e.querySelector(".gotoLine");
    if (t) {
      let e = t.href.split("#").pop();
      let n = e.split(":");
      e = {
        line: n[0] - 1,
        ch: n[1] - 1
      };
      t.addEventListener("click", t => {
          t.preventDefault();
          t.stopPropagation();
          Layout.editors.js.editor.focus();
          Layout.editors.js.editor.doc.setCursor(e)
        }
      )
    }
  }

  updateSummary() {
    let e = this.element.output.querySelectorAll(".log");
    let t = this.element.output.querySelectorAll(".info");
    let n = this.element.output.querySelectorAll(".error");
    let i = this.element.output.querySelectorAll(".warn");
    let r = e => e.length >= 1 ? "active" : "";
    this.element.summary.innerHTML = `\n    <span class="log ${r(e)}">\n      <svg height="12" version="1.1" viewBox="0 0 24 24"><g stroke-linecap="round" stroke-width="1.8" fill="none" stroke-linejoin="round"><circle cx="12" cy="12" r="10"></circle><line x1="12" x2="12" y1="8" y2="12"></line><line x1="12" x2="12.01" y1="16" y2="16"></line></g></svg>${e.length}\n    </span>\n    <span class="info ${r(t)}">\n      <svg height="12" version="1.1" viewBox="0 0 24 24"><g stroke-linecap="round" stroke-width="2" fill="none" stroke-linejoin="round"><circle cx="12" cy="12" r="10"></circle><line x1="12" x2="12" y1="8" y2="12"></line><line x1="12" x2="12.01" y1="16" y2="16"></line></g></svg>${t.length}\n    </span>\n    <span class="warn ${r(i)}">\n      <svg height="12" version="1.1" viewBox="0 0 24 24"><g stroke-linecap="round" stroke-width="2" fill="none" stroke-linejoin="round"><path d="M10.29 3.86l-8.47 14.14 -7.80912e-08 1.35236e-07c-.552351.956547-.224686 2.17975.73186 2.7321 .297614.171855.634491.264121.97814.267898h16.94l-1.12917e-07 1.24109e-09c1.1045-.0121397 1.99004-.917357 1.9779-2.02186 -.00377709-.343648-.0960428-.680526-.267898-.97814l-8.47-14.14 1.92952e-08 3.18097e-08c-.572861-.944407-1.80285-1.2456-2.74726-.672744 -.275097.166869-.505875.397647-.672744.672744Z"></path><line x1="12" x2="12" y1="9" y2="13"></line><line x1="12" x2="12.01" y1="17" y2="17"></line></g></svg>${i.length}\n    </span>\n    <span class="error ${r(n)}">\n      <svg height="12" version="1.1" viewBox="0 0 24 24"><g stroke-linecap="round" stroke-width="2" fill="none" stroke-linejoin="round"><polygon points="7.86,2 16.14,2 22,7.86 22,16.14 16.14,22 7.86,22 2,16.14 2,7.86 7.86,2"></polygon><line x1="12" x2="12" y1="8" y2="12"></line><line x1="12" x2="12.01" y1="16" y2="16"></line></g></svg>${n.length}\n    </span>\n    `;
  }

  format(e) {
    return e = (e = e.replace(/\[object\ Object\]\ /gi, "")).replace(/\[([\s\S]*)\]/im, "$1");
  }
}

// run
window.ConsoleManager = new DevlabConsole();

if (window) {
  customConsole(window)
}
