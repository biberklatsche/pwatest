import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({providedIn: "root"})
export class LocationService {

    private lastErrorSubject: BehaviorSubject<any> = new BehaviorSubject<any>(undefined);

    init() {
        if ( navigator.permissions && navigator.permissions.query) {
            //try permissions APIs first
            navigator.permissions.query({ name: 'geolocation' }).then((result) => {
                // Will return ['granted', 'prompt', 'denied']
                const permission = result.state;
                if ( permission === 'granted' || permission === 'prompt' ) {
                    this._onGetCurrentLocation();
                }
            });
        } else if (navigator.geolocation) {
            //then Navigation APIs
            this._onGetCurrentLocation();
        }
    }

    getLastError$(): BehaviorSubject<string> {
        return this.lastErrorSubject;
    }

    private _onGetCurrentLocation () {
        const options = {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
        };
        navigator.geolocation.getCurrentPosition( (position) => {
            //use coordinates
            const marker = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            console.log({marker});
        },  (error) => {
            //error handler here
            console.log({error});
            this.lastErrorSubject.next(error);
        }, options)
    }
}
