// db.ts
import Dexie, { Table } from 'dexie';

export interface TodoList {
    id?: number;
    title: string;
}

export class AppDB extends Dexie {
    todoLists!: Table<TodoList, number>;

    constructor() {
        super('MyPwaIndexedDB');
        this.version(1).stores({
            todoLists: '++id'
        });
        this.on('populate', () => this.populate());
    }

    async populate() {}

    async resetDatabase() {
        await db.transaction('rw', 'todoLists', () => {
            this.todoLists.clear();
            this.populate();
        });
    }
}

export const db = new AppDB();
