import {Component, isDevMode} from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';
import {SwUpdate} from '@angular/service-worker';
import {liveQuery} from 'dexie';
import {db, TodoList} from './db';
import {LocationService} from './location.service';
import {takeUntilDestroyed} from '@angular/core/rxjs-interop';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styles: []
})
export class AppComponent {
    title = 'my-pwa';
    currentLocationError: any;

    public storage: Subject<{ usage: number, quota: number, error?: string }> = new BehaviorSubject({
        usage: 0,
        quota: 0
    });
    public versionType: Subject<string> = new BehaviorSubject('');
    public isInDevMode: boolean = true;
    public iosDbs: string[] = [];

    listName = 'My new list';
    todoLists$ = liveQuery(() => db.todoLists.toArray());

    constructor(
        private swUpdate: SwUpdate,
        private locationService: LocationService
    ) {
        this.recalculateStorageUsage();
        this.isInDevMode = isDevMode();
        if (this.swUpdate.isEnabled) {
            this.swUpdate.versionUpdates.subscribe((s) => {
                this.versionType.next(s.type);
                if (s.type === 'VERSION_READY') {
                    if (confirm('Eine neue Version ist verfügbar. Möchtest du das Update herunterladen?')) {
                        window.location.reload();
                    }
                }
            });
        }

        this.locationService.init();
        this.locationService.getLastError$()
            .pipe(
                takeUntilDestroyed()
            )
            .subscribe((value: any) => {
                this.currentLocationError = value;
            })
    }

    private recalculateStorageUsage() {
        if (navigator && navigator.storage && navigator.storage.estimate) {
            navigator.storage.estimate()
                .then(result => this.storage.next({
                    usage: (result.usage || 0) / 1024,
                    quota: (result.quota || 0) / 1024 / 1024
                }));
        } else {
            this.storage.next({usage: 0, quota: 0, error: 'storage is not supported'});
        }
    }

    async addNewList() {
        console.log('add New List');
        await db.todoLists.add({
            title: this.listName,
        });
    }

    identifyList(index: number, list: TodoList) {
        return `${list.id}${list.title}`;
    }
    async resetDatabase() {
        await db.resetDatabase();
    }
}
